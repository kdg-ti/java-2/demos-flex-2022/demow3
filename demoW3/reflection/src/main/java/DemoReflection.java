import model.Student;
import reflection.Reflector;

public class DemoReflection {
    public static void main(String[] args) {
        Reflector.introspection(Student.class);
        System.out.println("**************************************************************************");
        Reflector.introspection(Integer.class);
        System.out.println("**************************************************************************");
        Reflector.modification(Student.class);
        System.out.println("**************************************************************************");
        Reflector.annotation(Student.class);
    }
}
